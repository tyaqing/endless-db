type Stores = {
    [key: string]: string
};

class EndlessDB {
    private db: IDBDatabase | undefined = undefined;
    private version = 1
    dbName: string = ''
    constructor(dbName: string) {
        this.dbName = dbName
    }
    init() {
        let self = this
        return new Promise((resolve, reject) => {
            const request = window.indexedDB.open(this.dbName, this.version);
            request.onerror = function (event) {

                // Do something with request.errorCode!
                const {error} = (event.target as IDBOpenDBRequest)
                // 当遇到代码回退造成的版本错误,需要重新打开数据库
                if (error && error.message.indexOf('is less than the existing version')) {
                    const res = error.message.match(/(\d+)/g)
                    if (res) {
                        self.version = Number(res[1])
                        resolve(self.init())
                    }
                    console.log(res)
                    // db的版本比打开的高
                    console.log('db的版本比打开的高', request)
                    // self.init()
                }
                reject(error)
            };
            request.onsuccess = (event) => {
                console.log('request.onsuccess')
                self.db = request.result
                resolve(0);
            };
            request.onupgradeneeded = (event) => {
                console.log('request.onupgradeneeded')
                // 保存 IDBDataBase 接口
                const {result} = (event.target as IDBOpenDBRequest)
                console.log('event.oldVersion', event.oldVersion)
                switch (event.oldVersion) { // 现有的 db 版本
                    case 0:
                        // 版本 0 表示客户端没有数据库
                        // 执行初始化
                        self.db = result;
                        this._createObjectStore('customers')
                        break
                    case 1:
                        // 客户端版本为 1
                        // 更新
                        break;
                    default:
                        console.log('default')
                        self.db = result;
                        this._createObjectStore('customers')
                }

                // 为该数据库创建一个对象仓库
                // var objectStore = db.createObjectStore("name", { keyPath: "myKey" });
            };
        })

    }
    _createObjectStore(name: string) {
        const objectStore = this.db!.createObjectStore(name, {keyPath: "ssn", autoIncrement: true});
        // 建立一个索引来通过姓名来搜索客户。名字可能会重复，所以我们不能使用 unique 索引
        objectStore.createIndex("name", "name", {unique: false});
        // 使用邮箱建立索引，我们向确保客户的邮箱不会重复，所以我们使用 unique 索引
        objectStore.createIndex("email", "email", {unique: true});
    }

    stores(stores: Stores) {
        Object.keys(stores).forEach(key => {
            console.log(stores[key])
        })
    }
    async add(data: any) {
        if (!this.db) {
            await this.init()
        }
        if (!Array.isArray(data)) {
            data = [data]
        }
        // // 建立一个对象仓库来存储我们客户的相关信息，我们选择 ssn 作为键路径（key path）
        // // 因为 ssn 可以保证是不重复的
        // console.log(EndlessDB.db)
        const customerData = data;
        const transaction = this.db!.transaction("customers", "readwrite")
        const customerObjectStore = transaction.objectStore("customers");
        customerData.forEach(function (customer: any) {
            customerObjectStore.add(customer);
        });
    }
    async get() {
        if (!this.db) {
            await this.init()
        }
        return new Promise((resolve, reject) => {
            const transaction = this.db!.transaction("customers")
            const objectStore = transaction.objectStore("customers");
            const request = objectStore.getAll();
            request.onerror = function (event) {
                // 错误处理!
            };
            request.onsuccess = function (event) {
                // 对 request.result 做些操作！
                resolve(request.result)
            };

        })
    }
    async delete(target: string) {
        if (!this.db) {
            await this.init()
        }
        return new Promise((resolve, reject) => {
            const transaction = this.db!.transaction("customers", 'readwrite')
            const objectStore = transaction.objectStore("customers");
            // const ssnIndex = objectStore.index('name')
            // const request = ssnIndex.getKey(target)
            const request = objectStore.delete(+target);
            request.onsuccess = function (event) {
                console.log('onsuccess', event)
                // let id = request.result;
                // let deleteRequest = objectStore.delete(id!);
                // 删除成功！
                resolve(0)
            };
            request.onerror = function (event) {
                console.log('onerror', event)
                // 删除成功！
                reject(event)
            };
        })
    }
}

function parseStoresRule() {

}

const note = document.querySelector('#note')
const app = document.querySelector('#app')

note && note.addEventListener('click', (event) => {
    const r = Math.floor(Math.random() * 100000)
    EDB.add([
        {ssn: r, name: "Bill", age: 35, email: `${r}bill@company.com`},
    ])
    print()
    event.stopPropagation()
})

app && app.addEventListener('click', (e) => {
    const id = (e.target as HTMLDataElement).innerText
    console.log(id)
    EDB.delete(id)
    print()
})

const EDB = new EndlessDB('Test');
EDB.stores({
    books: '++id,name,count,price'
})

print();
function print() {
    if (app) {
        while (app.firstChild) {
            app.firstChild.remove()
        }
    }
    EDB.get().then((data) => {

        (data as []).forEach(item => {
            const li = document.createElement('li')
            li.classList.add('delete')
            li.innerText = `${item['ssn']}`
            app && app.appendChild(li)
        })
    })
}


//TODO 需要考虑回退的问题
